#!/usr/bin/expect --
#
# Author: Michael Ambrus (ambrmi09@gmail.com)
# Butt-Ugly-Software (BUS)
# 2014-01-31

#== Expected input positional arguments:  ===========
#1 Server
#2 JID
#3 Password
#4 Room
#4 Room-password

#== Some variables ===================================
	set HomeDir [file dirname $argv0]
	set Me [file tail $argv0]
	set TNow {}

#== Internal and debug variables ====================
	exp_internal 0
	log_user 0
	set logfile [open "/tmp/$Me.log" "a"]
	# Uncomment the following if you want to debug internal
	exp_internal -f "/tmp/$Me-int.log" 0
	# In bot mode we don't need echoing. For debugging,
	# disable below or set the opposite (stty cooked echo)
	#stty raw -echo

#== Get some external vars ===========================
global env

if { [info exists env(XHOST)] && $env(XHOST) != "" } {
	set XHOST		$env(XHOST)
}
if { [info exists env(JID)] && $env(JID) != "" } {
	set JID			$env(JID)
}
if { [info exists env(XPASS)] && $env(XPASS) != "" } {
	set XPASS		$env(XPASS)
}
if { [info exists env(XROOM)] && $env(XROOM) != "" } {
	set XROOM		$env(XROOM)
}
if { [info exists env(XRPASS)] && $env(XRPASS) != "" } {
	set XRPASS		$env(XRPASS)
}
if { [info exists env(HAS_TTY)] && $env(HAS_TTY) != "" } {
	set HAS_TTY		$env(HAS_TTY)
}

#--- Env-vars for proxy stuff
if { [info exists env(XMPP_USE_PROXY)] && $env(XMPP_USE_PROXY) != "" } {
	set XMPP_USE_PROXY	$env(XMPP_USE_PROXY)
	set USE_PROXY	"yes"
} else {
	set USE_PROXY	"no"
}
if { [info exists env(XMPP_PROXY_ADDRESS)] && $env(XMPP_PROXY_ADDRESS) != "" } {
	set XMPP_PROXY_ADDRESS		$env(XMPP_PROXY_ADDRESS)
}
if { [info exists env(XMPP_PROXY_PASSWORD)] && $env(XMPP_PROXY_PASSWORD) != "" } {
	set XMPP_PROXY_PASSWORD		$env(XMPP_PROXY_PASSWORD)
}
if { [info exists env(XMPP_PROXY_PORT)] && $env(XMPP_PROXY_PORT) != "" } {
	set XMPP_PROXY_PORT		$env(XMPP_PROXY_PORT)
}
if { [info exists env(XMPP_PROXY_TYPE)] && $env(XMPP_PROXY_TYPE) != "" } {
	set XMPP_PROXY_TYPE		$env(XMPP_PROXY_TYPE)
}
if { [info exists env(XMPP_PROXY_USER)] && $env(XMPP_PROXY_USER) != "" } {
	set XMPP_PROXY_USER		$env(XMPP_PROXY_USER)
}

#== Functions ========================================
source $HomeDir/shared.tcl
source $HomeDir/un_ansi.tcl

proc xmpp_set_setting { setting value } {
	log_this "Set setting: $setting $value"
	send "/SET $setting $value\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed set setting: $setting"
		}
		"$setting"	{
			log_this "Setting OK: $setting"
		}
	}
}

proc xmpp_clear_setting { setting } {
	log_this "Clearing setting: $setting"
	send "/SET -clear $setting\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed clearing setting: $setting"
		}
		"$setting"	{
			log_this "Clearing OK: $setting"
		}
	}
}

#=====================================================
	log_this "*** Starting $Me with tty=[tty_attached]***"

	set argc [llength $argv]

	if { $argc == 5 } {
		set XHOST  [lindex $argv 0]
		set JID    [lindex $argv 1]
		set XPASS  [lindex $argv 2]
		set XROOM  [lindex $argv 3]
		set XRPASS [lindex $argv 4]
	} else {
		log_this "Not exactly 5 args to $Me. Relaying on envvars instead..."
	}

	log_this "Communicating with irssi as follows:"

	log_this "irssi -d --home=/dev --config=null"

	spawn irssi -d --home=/dev --config=null

	# In bot mode we don't need echoing. For debugging,
	# disable below or set the opposite (stty cooked echo)
	stty -echo
	set timeout 5

	expect {
		timeout {
			expect_abort 1 "Timeout: Irssi not responding (is it installed?)"
		}
		"\[(status)\]"	{
			log_this "irssi is up!"
		}
	}

	log_this "loading issi module: xmpp"
	send "/LOAD xmpp\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed loading module xmpp-irc"
		}
		-re "Loaded module.*xmpp"	{
			log_this "Module xmpp-irc found and loaded"
		}
	}


	#--------------------------------------------
	log_this "Entering proxy section..."
	xmpp_clear_setting	xmpp_use_proxy
	xmpp_clear_setting	xmpp_proxy_address
	xmpp_clear_setting	xmpp_proxy_password
	xmpp_clear_setting 	xmpp_proxy_port
	xmpp_clear_setting 	xmpp_proxy_type
	xmpp_clear_setting	xmpp_proxy_user
	if { $USE_PROXY	== "yes" } {
		log_this "Setting proxy configs"
		xmpp_set_setting	xmpp_use_proxy 		$XMPP_USE_PROXY
		xmpp_set_setting	xmpp_proxy_address	$XMPP_PROXY_ADDRESS
		xmpp_set_setting	xmpp_proxy_password	$XMPP_PROXY_PASSWORD
		xmpp_set_setting 	xmpp_proxy_port		$XMPP_PROXY_PORT
		xmpp_set_setting 	xmpp_proxy_type		$XMPP_PROXY_TYPE
		xmpp_set_setting	xmpp_proxy_user		$XMPP_PROXY_USER
	} else {
		log_this "Disabling proxy configuguration"
		xmpp_set_setting	xmpp_use_proxy 		OFF
	}
	#enable to debug:
	#interact

	#then:
	# source envsetu.sh
	# expect/xmppr-irssi-node.exp

	log_this "/XMPPCONNECT -host $XHOST $JID $XPASS"
	send "/XMPPCONNECT -host $XHOST $JID $XPASS\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed logging into $XHOST as $JID"
		}
		-re "Connection to.*$XHOST.*established"  {
			log_this "Logged into $XHOST as $JID"
		}
	}

	log_this "/JOIN $XROOM@conference.$XHOST/BABELFISH $XRPASS"
	send "/JOIN $XROOM@conference.$XHOST/BABELFISH $XRPASS\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed joining $XROOM@conference.$XHOST/BABELFISH 1(2)"
		}
		"\[teleport@confer\]"  {
			log_this "joined $XROOM@conference.$XHOST/BABELFISH"
		}
	}

	#Need to wash old messages away (XMPP standard feature is to keep a log)
	#Listen to a known echo, discard everything up until then

	set TNow [clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}]
	send "BABELFISH enters room: $TNow\r"
	expect {
		timeout {
			expect_abort 1 "Timeout: Failed joining $XROOM@conference.$XHOST/BABELFISH 2(2)"
		}
		"BABELFISH enters room: $TNow"  {
			log_this "joined $XROOM@conference.$XHOST/BABELFISH"
		}
	}

	if { $HAS_TTY == 0 } {
		# /dev/tty doesn't exist
		# probably in cron, batch, or at script
		close_on_eof 0
		log_this "PIPE detected"
		set timeout 3
	}
	interact {
		-o
		-re "(\[\[:digit:\]\]{2}:\[\[:digit:\]\]{2} .*$)" {
			set OSTR [un_ansi $interact_out(1,string)]
   			#regsub -all -- " +" ${txt} { } txt
			send_user "\[$OSTR\]"
			log_this "\[$OSTR\]"
		}
		-re "(\[\u001b\].37\[^<>\]*\[\u001b\].27m)" {
			log_this "###$interact_out(1,string)###"
		}
#		-re "(.*)" {
#			log_this "###$interact_out(1,string)###"
#		}
	}

	if { $HAS_TTY == 0 } {
		set timeout 1
		expect
	}

	exit 0

