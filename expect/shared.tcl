#!/usr/bin/tcl --
#
# Author: Michael Ambrus (ambrmi09@gmail.com)
# Butt-Ugly-Software (BUS)
# 2014-01-31

# Variable definition & placeholder
set HAS_TTY 1
# Note
# * [tty_attached] needs to be run once to set it truthfully. 
# * [tty_attached] will not work if script is in itself spawned and needs help
#   by exporting envirnonment variable HAS_TTY

proc log_this { string } {
	global logfile

	puts $logfile "$string"
	flush $logfile
}

proc expect_abort { ec estrg } {
	global Me

	log_this "======================================================"
	log_this "$Me: Error #$ec"
	log_this "$estrg"
	log_this "======================================================"
	send_user "$Me: Error (#$ec): $estrg\n"
	sleep 1
	exit $ec
}

# Determines if tty is attached or not (i.e. if we're part of a pipe)
# Note that it sets the global variable HAS_TTY. The point of this is
# when a spawned process dies, tty_spawn_id also gets false even if we're still
# attached to a pipe. By using the variable instead we know how the state was
# when the script was started. Note that the Expect manpages are wron about
# tty_spawn_id, that variable never exists.

proc tty_attached { } {
	global HAS_TTY
	global env

	if { [info exists env(HAS_TTY)] && $env(HAS_TTY) != "" } {
		set HAS_TTY		$env(HAS_TTY)
		return $HAS_TTY
	}

	if { [catch { system {[[ -t 0 ]]} } error] } {
		set HAS_TTY 0
	} else {
		set HAS_TTY 1
	}
	return $HAS_TTY
}
