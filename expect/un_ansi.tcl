#!/usr/bin/tcl --
#
# Author: Michael Ambrus (ambrmi09@gmail.com)
# Butt-Ugly-Software (BUS)
# 2014-01-31

proc un_ansi {data} {
    # --------------------------------------------------------------------------
    # remove ansi escape sequences
    # --------------------------------------------------------------------------
    set txt {}
    while {[string length ${data}]} {
      set match { }
      switch -regexp -- ${data} {
        {^\x1b(\[|\(|\))[;?0-9]*[0-9A-Za-z]} {
             regexp -- {^\x1b(\[|\(|\))[;?0-9]*[0-9A-Za-z]} ${data} match
             append txt "\n"
           }
        {^(.+?)\x1b} {
             regexp -- {^(.+?)\x1b} ${data} UNUSED match
             # handle special escape sequences
             regsub -all -- {\\([\\\[\]])} ${match} {\1} raw_match
             append txt ${raw_match}
           }
        {^\x1b} {
             # do nothing
           }
        default {
             set match ${data}
             append txt ${match}
           }
       }
       set data [string range ${data} [string length ${match}] end]
   }

   # remove white spaces not needed anymore
   regsub -all -- "\t+" ${txt} { } txt
   regsub -all -- " +" ${txt} { } txt
   regsub -all -- "\[\x01-\x1F\]" ${txt} {} txt
   regsub -all -nocase -- "\n+" [string trim ${txt}] "\n" txt
   return ${txt}
}
