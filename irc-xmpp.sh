#! /bin/bash
# This script is a wrapper for the irc-xmpp-interact.exp
# Use this script instead of calling irc-xmpp-interact.exp, it's safer and
# handles new settings better

pushd $(dirname $(readlink -f $0))
source envsetup.sh
reset; clear

if [ -t 0 ]; then
	export HAS_TTY=1
else
	export HAS_TTY=0
fi
export HAS_TTY

irc-xmpp-interact.exp

popd > /dev/null
