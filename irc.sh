#! /bin/bash
# This script is a wrapper for the xmpp-node which helps out with the
# environment and does some more ANSI scrubbing just like the teleporter
# does.

pushd $(dirname $(readlink -f $0))
source envsetup.sh
reset; clear

if [ -t 0 ]; then
	export HAS_TTY=1
else
	export HAS_TTY=0
fi
export HAS_TTY

irc.exp

popd > /dev/null
