# Environment settings common to all scripts in this package.
# Intended to be sourced from scripts in this package only.

if [ "X${IRCXMPP_ENV_SET}"="Xy" ]; then
# No need to do this if it's already done
	if ! [ -f ${HOME}/.irc-xmpp/accounts ]; then
		echo "Error: irc-xmpp not setup (accounts missing)." 1>&2
		exit 1
	fi

	RDIR=$(dirname $(readlink -f $0))
	source ${RDIR}/.env_common

	export IRCXMPP_ENV_SET='y'
	export PNAME=ircxmpp

	# Where log-files e.t.a. go
	export IRCXMPP_TMPDIR=/tmp/ircxmpp

	export LC_ALL=en_CA.UTF-8

	# Path to executables
	set_path_ifneeded ${RDIR}/expect

	# Get user settings and set if not set
	# Parse one line at a time
	for L in $(
		cat ${HOME}/.irc-xmpp/accounts | \
		grep -vE '^#' | \
		grep -vE '^[[:space:]]*$'
	); do
		E=$(echo $L | cut -d"=" -f1)
		V=$(eval echo \$$E)
		if [ "X$V" == "X" ]; then
			#echo "Setting value for envvar $E"
			eval $(echo "export $L")
		fi
	done
fi
