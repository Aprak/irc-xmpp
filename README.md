## Installation ##

### Setup ###

Create a link to any of the shell-scripts in the root-directory of this
project into somewhere you have PATH to (~/bin/ /usr/local/bin e.t.c.).
Note: scripts in root-directory only

### Configuration ###
Create a file ~/.irc-xmpp/accounts with the settings below filled in
(note that you can keep comments and whitespace).


    #IRC settings
    #============
    IHOST=fqdn.irc.host
    NICK=yournick
    CHAN=yourchan

    #xmpp settings
    #============

    XHOST=fqdn.xmpp.host
    JID=botname@~/.irc-xmpp/accounts
    XPASS=****
    XROOM=YourPreMadeRoom
    XRPASS=*****

    #xmpp PROXY settings
    #===================
    XMPP_USE_PROXY=ON
    XMPP_PROXY_ADDRESS=fqdn.proxy.host
    XMPP_PROXY_USER=YourNetAccount
    XMPP_PROXY_PASSWORD=****
    XMPP_PROXY_PORT=8080
    XMPP_PROXY_TYPE=http

### Dependencies ###
#### External XMPP server & room(s) ####
Note that the xmpp host must be a server on the outside if you're going to
connect via for example a phone. You must have pre-made a room there and you
*should* control it's access by assigning a password to it. This room is
intended for you and your eyes only. Everything written in it will be
carried into IRC as if it was written by you.

#### Why using a room at all? ####

* A room serves as a convenient way to achieve connection-robust multi-casting.
* Rooms also serves a BNC or notepad. Especially important with phones as
  connections go up and down (you don't want to miss part of the conversation
  concerning you.

### On your host ###
The following needs to be on your machine:

* [irssi](http://www.irssi.org/). Any installation method will do as long as you have it your PATH. Expect (the language behing this bridge) takes away any pain with intra-binary dependencies.
    * Except one: No rule without an exception eh? The [irssi XMPP](http://cybione.org/~irssi-xmpp/) irssi-plugin needs to be installed. The method of having dependencies contradicts much of the purpose behind his project and we could had written a dedicated XMPP manager for another binary, but at the time this was deemed the most proficient. Also considering [XMPP-PGP](http://www.xmpp.org/extensions/xep-0027.html) which is part of XMPP-standard, but **not** IRC. I.e. this way we can get IRC encrypted in a standardized way even though PGP encryption is not part of the [IRC RFC](http://tools.ietf.org/html/rfc2812).
    * PGP may be needed from a security point of view. Even though usage of [ssl connections](http://wiki.xmpp.org/web/Securing_XMPP) is encouraged it is not mandatory.
    * Keep in mind that the external room keeps a record of what's written. If the key is lost or door left open, your really f**ed.
    * Room or Room-key can be compromised in other ways too. For example by someone with administrative rights on the machine hosting your external room. Did you know that [ejabberd 2](http://www.ejabberd.im/) for example stores all accounts and password in clear-text in a semi-binary file? You just need to know where to look...
* The [Expect](http://www.tcl.tk/man/expect5.31/expect.1.html) language. Any installation method will do as long as you have it your PATH. Note that even though this is a extention to TCL, and the syntax and language constructs are TCL (for good and for bad), you usually don't install TCL but Expect. Let your packet manager figure out what's needed and what's not. On Debian/Ubuntu which I use, Expect comes as a package of it's own without dependencies to TCL. 

### Test your setup ###


Use the scripts irc.sh and xmpp.sh respectively to test each server/proxy
setup. In interactive mode you should be left with a blank screen, but which
directly interacts with connected session. Non-interactively you can send
messages from piped stdin.

Whence this is proven to work, you can invoke the bridge: irc-xmpp.sh

Usage
=====

irc-xmpp.sh is meant to be started and then left alone. Preferably in a
screen session. For example like this:

screen -S ircbridge
path/to/irc-xmpp.sh

Additional you can use the two subscripts: irc.sh and xmpp.sh.
Interactive use and function is described under "Test setup". A cool feature
is that they can be used for piping message. In conjunction that specific
environment variables can be overloaded you can write cool scripts to inject
text into irc, like the following example:

## MatTanten script ##

This script can be run as a cron-job which is rather cool.

	#!/bin/bash

	TERM=${TERM-"linux"}
	if [ $TERM == "dumb" ]; then
		TERM=linux
	fi

	export TERM
	export NICK=MatTanten

	(dagens.sh -c glasgow; dagens.sh -c hilda) | irc.sh

### Notes about headless services like the above ###

1. TERM environment variable, it's needed for expect to work when invoked as a service, for example as a crontab job.

2. The overloaded NICK (you can overload any, but it rarely makes sense to overload more than this and perhaps the CHAN)



### Contribution guidelines ###

* More managers:
As this is a generic method for bridging chat networks, whatever network you have a console application for can be used. You can even write your own binaries if you feel like it. The only thing that's required is:
    * It communicates via **stdin/stdout**. I.e. no terminal interaction. If suck exists, strip it (see the managers here for examples). This is a main corner-stone in the project. Only by using pipes can a switching-board easily be achieved. That's what this project really mainly is: a switching board with the added ability to get around fire-walls. The dedicated xmpp- and irc-managers are just examples and can be replaced either by other manages for the same or other networks.
    * All settings affecting any kind of behavior should be set via environment variables. Implicitly: For settings to have affect, your service needs to be restarted.
    * The program should need no interactive help to set up it's part of the communication. It must be able to do this completely by itself, no matter concepts like "channels", "roster" or whatever. Use the environment variables to refine an intended connection (invent your own if needed) and wrap it into a super-script (one for each connection). Compare with a speed-dial if you like... The point is: if you need a 1-to-N, M-to-1 or M-to-N service, flexibility is not the priority.


* Code review:
Yes please.

* Other guidelines:
None.

### Who do I talk to? ###

* [Me](mailto:aprak.ofatys@gmail.com)


### General quirks... and stuff... ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* External xmpp-server security: Consider having a machine of your own running within your own physical doors. At least until PGP is fully implemented for this bridge. Considerations:
    * Even with dial-up connections there are ways to have good enough QoS and availability.
    * Both IRC and XMPP were designed to be connection tolerant. 
    * Fixed DNS is not a real problem since a decade. Ask around how-to, or ask [Me](mailto:aprak.ofatys@gmail.com) if you have no idea where to start. 
    * The only thing you need to check it that your IP-provider does not prevent remote-side initiated TCP sessions. This has started to become reality and is a real bug-in-the-neck if you ask me... 